//
//  ImageModel.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 13.11.2021.
//

import Foundation

struct ImageModel: Codable {
    let urls: SizeModel
}

struct SizeModel: Codable {
    let raw,
        full,
        regular,
        small,
        thumb: String
}

