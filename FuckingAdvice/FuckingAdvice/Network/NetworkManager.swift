//
//  NetworkManager.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 12.11.2021.
//

import Foundation
import Alamofire

enum Encoding {
    case `default`
    case query
    
    var type: ParameterEncoding {
        switch self {
        case .default:
           return JSONEncoding()
        case .query:
            return URLEncoding(destination: .queryString)
        }
    }
}

enum SchemeNetwork: String {
    case advice = "https://fucking-great-advice.ru/api/random"
    case img = "https://api.unsplash.com/photos/random"
    
    var key: String {
        switch self {
        case .advice:
            return "none"
        case .img:
            return "DM7Dbs7Xw_7LLPMfYRC3gangCB3pd-DOucfUiPwL0MI"
        }
    }
}

protocol NetworkManager: AnyObject {
    //swiftlint:disable function_parameter_count
    func doRequest<T>(with target: SchemeNetwork,
                      parameters: [String: String]?,
                      encoding: Encoding,
                      _ modelType: T.Type,
                      _ onSuccess: ((T) -> Void)?,
                      _ onError: (() -> Void)?) where T: Decodable
}

extension NetworkManager {
    
    func doRequest<T>(with target: SchemeNetwork,
                      parameters: [String: String]? = nil,
                      encoding: Encoding = .default,
                      _ modelType: T.Type,
                      _ onSuccess: ((T) -> Void)?,
                      _ onError: (() -> Void)?) where T: Decodable {
        
        AF.request(target.rawValue, method: .get, parameters: parameters, encoding: encoding.type).responseDecodable(of: T.self) { response in
            switch response.result {
            case let .success(result):
                onSuccess?(result)
            case let .failure(error):
                print(error.errorDescription ?? "")
                onError?()
            }
        }
    }
}
