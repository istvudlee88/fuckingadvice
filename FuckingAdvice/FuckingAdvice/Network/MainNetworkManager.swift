//
//  MainNetworkManager.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 13.11.2021.
//

import Foundation

protocol MainNetworkManagerProtocol: NetworkManager {
    var imageManager: ImageLoadManagerProtocol { get }
    func getAdvice(onSuccess: ((AdviceModel) -> Void)?, onError: (() -> Void)?)
    func getImageData(onSuccess: ((ImageModel) -> Void)?, onError: (() -> Void)?)
}

class MainNetworkManager: MainNetworkManagerProtocol {
    // MARK: - public props
    var imageManager: ImageLoadManagerProtocol {
        //swiftlint:disable force_try
        try! Containers.managersContainer.resolve()
    }
    // MARK: - private methods
    func getAdvice(onSuccess: ((AdviceModel) -> Void)?, onError: (() -> Void)?) {
        doRequest(with: .advice, AdviceModel.self) { data in
            onSuccess?(data)
        } _: {
            onError?()
        }
    }
    
    func getImageData(onSuccess: ((ImageModel) -> Void)?, onError: (() -> Void)?) {
        let target: SchemeNetwork = .img
        let params = ["client_id": "\(target.key)"]
        doRequest(with: target, parameters: params, encoding: .query, ImageModel.self) { data in
            onSuccess?(data)
        } _: {
            onError?()
        }
    }
}
