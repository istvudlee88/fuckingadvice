//
//  Test.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 05.12.2021.
//

import SwiftUI

struct Sample: View {
    @Namespace var ns
    var body: some View {
        HStack(alignment: .myCenter) {
            Rectangle().fill(Color.blue)
                .frame(width: 50, height: 50)
            Rectangle().fill(Color.green)
                .frame(width: 30, height: 30)
            Rectangle().fill(Color.red)
                .frame(width: 40, height: 40)
                .alignmentGuide(.myCenter, computeValue: { dim in
                    return dim[.myCenter] - 20
                })
        }.border(Color.black)
    }
}



struct Test_Previews: PreviewProvider {
    static var previews: some View {
        Sample()
    }
}

enum MyCenterID: AlignmentID {
    static func defaultValue(in context: ViewDimensions) -> CGFloat {
        context.height / 2
    }
}

extension VerticalAlignment {
    static let myCenter: VerticalAlignment = VerticalAlignment(MyCenterID.self)
}



