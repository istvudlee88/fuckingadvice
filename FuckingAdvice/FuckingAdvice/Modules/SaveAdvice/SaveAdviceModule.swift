//
//  SaveAdviceModule.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 28.11.2021.
//

import SwiftUI

struct SaveAdviceModule: View {
    @Binding var text: String
    @Binding var isFocused: Bool
    var placeHolder: String
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Напиши свой совет")
                .font(Font.bold(30))
                .foregroundColor(Color(.black))
            TextFieldMC(text: $text, isFocused: $isFocused, placeHolder: placeHolder)
                .padding(.leading, 16)
                .border(Color(.black), width: 1)
                .frame(height: 54)
            Spacer()
            Text(text)
            Button("СОХРАНИТЬ") {
                print("save")
            }
            .padding(32)
            .frame(maxWidth: .infinity, maxHeight: 55, alignment: .center)
            .foregroundColor(Color(.black))
            .background(Color(.green))
            .cornerRadius(8)
        }.padding(16)
    }
}

// MARK: - Custom UITextField Representable
struct TextFieldMC: UIViewRepresentable {
    @Binding var text: String
    @Binding var isFocused: Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var placeHolder: String

    func makeUIView(context: UIViewRepresentableContext<TextFieldMC>) -> UITextField {
        let textFiled = UITextField(frame: .zero)
        textFiled.delegate = context.coordinator
        textFiled.inputAccessoryView = makeToolbar {
            self.presentationMode.wrappedValue.dismiss()
        }
        textFiled.placeholder = placeHolder
        textFiled.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return textFiled
    }
    
    func makeCoordinator() -> TextFieldMC.Coordinator {
        Coordinator(text: $text, isFocused: $isFocused)
    }
    
    func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
        isFocused ? uiView.becomeFirstResponder() : uiView.resignFirstResponder()
    }
    
    class Coordinator: NSObject, UITextFieldDelegate {
            @Binding var text: String
            @Binding var isFocused: Bool

            init(text: Binding<String>, isFocused: Binding<Bool>) {
                _text = text
                _isFocused = isFocused
            }

            func textFieldDidChangeSelection(_ textField: UITextField) {
                text = textField.text ?? ""
            }

            func textFieldDidBeginEditing(_ textField: UITextField) {
                DispatchQueue.main.async {
                   self.isFocused = true
                }
            }

            func textFieldDidEndEditing(_ textField: UITextField) {
                DispatchQueue.main.async {
                    self.isFocused = false
                }
            }

            func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                textField.resignFirstResponder()
                return false
            }
        }
}

func makeToolbar(actionHandler: (() -> Void)?) -> UIView {
    let toolBar = UIToolbar(frame: .init(x: 0, y: 0, width: 0, height: 44))
    let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let button = BarButtonItem {
        actionHandler?()
    }
    toolBar.items = [flexButton, button]
    
    return toolBar
}

class BarButtonItem: UIBarButtonItem {
    private var actionHandler: (() -> Void)?
    
    convenience init(actionHandler: (() -> Void)?) {
        self.init(title: "Done", style: .done, target: nil, action: #selector(actionButton))
        self.target = self
        self.actionHandler = actionHandler
    }
    
    @objc private func actionButton() {
        actionHandler?()
    }
}

