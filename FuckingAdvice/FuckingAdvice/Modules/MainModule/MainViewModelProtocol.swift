//
//  MainViewModelProtocol.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 13.11.2021.
//

import Foundation

protocol MainViewModelProtocol: ObservableObject {
    var data: MainViewModel.Model? { get set }
    var isActiveIndicator: Bool { get set }
    func fetchData()
}
