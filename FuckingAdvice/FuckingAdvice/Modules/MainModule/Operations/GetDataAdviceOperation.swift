//
//  GetDataAdviceOperation.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 14.11.2021.
//

import Foundation

class GetDataAdviceOperation: Operation {
    // MARK: - private props
    private var wrapperState: StateWrapper
    
    // MARK: - lifecycle
    init(wrapperState: StateWrapper) {
        self.wrapperState = wrapperState
    }
    
    override func main() {
        guard !isCancelled else { return }
        
        let operation = OperationQueue()
        operation.maxConcurrentOperationCount = 1
        
        let getAdviceOperation = GetAdviceOperation(wrapper: wrapperState)
        operation.addOperation(getAdviceOperation)
        operation.waitUntilAllOperationsAreFinished()
        
        let getImageOperation = GetImageDataOperation(wrapper: wrapperState)
        getImageOperation.completionBlock = { [weak self] in
            self?.wrapperState.isDataGetting = true
        }
        operation.addOperation(getImageOperation)
        operation.waitUntilAllOperationsAreFinished()
    }
}
