//
//  GetImageDataOperation.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 14.11.2021.
//

import Foundation

class GetImageDataOperation: AsyncOperation {
    // MARK: - private props
    private let wrapper: StateWrapper
    // swiftlint:disable force_try
    private let networkManager: MainNetworkManagerProtocol = try! Containers.managersContainer.resolve()
    
    // MARK: - lifecycle
    init(wrapper: StateWrapper) {
        self.wrapper = wrapper
    }
    override func main() {
        guard !isCancelled else { return }
        networkManager.getImageData { [weak self] model in
            let url = URL(string: model.urls.regular)
            self?.networkManager.imageManager.downloader(url: url) { [weak self] data in
                self?.wrapper.imageData = data
                self?.state = .finished
            } onError: { [weak self] _ in
                self?.state = .finished
            }
        } onError: { [ weak self] in
            self?.state = .finished
        }
    }
}
