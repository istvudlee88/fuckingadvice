//
//  GetAdviceOperation.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 14.11.2021.
//

import Foundation

class GetAdviceOperation: AsyncOperation {
    // MARK: - private props
    private let wrapper: StateWrapper
    //swiftlint:disable force_try
    private let networkManager: MainNetworkManagerProtocol = try! Containers.managersContainer.resolve()
    
    // MARK: - lifecycle
    init(wrapper: StateWrapper) {
        self.wrapper = wrapper
    }
    
    override func main() {
        guard !isCancelled else { return }
        networkManager.getAdvice { [weak self] model in
            self?.wrapper.text = model.text
            self?.state = .finished
        } onError: { [weak self] in
            self?.state = .finished
        }
    }
}
