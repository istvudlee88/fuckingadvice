//
//  MainViewModel.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 13.11.2021.
//

import Foundation
import Combine

class MainViewModel: MainViewModelProtocol {
    struct Model {
        let text: String
        let dataImg: Data
    }
    
    // MARK: - private props
    //swiftlint:disable force_try
    private let networkManager: MainNetworkManagerProtocol = try! Containers.managersContainer.resolve()
    
    // MARK: - public props
    @Published var data: Model?
    @Published var isActiveIndicator = false
    
    // MARK: - public methods
    func fetchData() {
        isActiveIndicator = true
        let wrapperState = StateWrapper()
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        
        DispatchQueue.global(qos: .utility).sync {
            let getDataAdviceOperation = GetDataAdviceOperation(wrapperState: wrapperState)
            operationQueue.addOperation(getDataAdviceOperation)
            getDataAdviceOperation.completionBlock = { [ weak self] in
                DispatchQueue.main.async {
                    if wrapperState.isDataGetting {
                        self?.isActiveIndicator = false
                        self?.data = .init(text: wrapperState.text ?? "null", dataImg: wrapperState.imageData ?? Data())
                    }
                }
            }
        }
    }
}
