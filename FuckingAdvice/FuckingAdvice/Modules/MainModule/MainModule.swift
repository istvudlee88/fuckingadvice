//
//  MainModule.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 13.11.2021.
//

import SwiftUI

struct MainModule: View {
    @ObservedObject var viewModel = MainViewModel()
    let width = UIScreen.main.bounds.width
    
    var body: some View {
        ZStack {
            ImageMain(data: viewModel.data?.dataImg)
            VStack {
                Spacer()
                ZStack {
                    Text(viewModel.data?.text ?? "")
                        .padding(16)
                        .frame(width: width, alignment: .center)
                        .font(Font.bold(46))
                        .foregroundColor(Color(.white))
                        .multilineTextAlignment(.leading)
                        .fixedSize()
                        .lineLimit(nil)
                    Spacer()
                    if viewModel.isActiveIndicator {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: .black))
                    }
                }
                Spacer()
                if !viewModel.isActiveIndicator {
                    Buttons()
                        .padding(32)
                        .frame(width: width, alignment: .center)
                }
            }
        }.onAppear {
            //            viewModel.fetchData()
        }.onTapGesture {
            //            viewModel.fetchData()
        }.padding(.horizontal)
    }
}

struct ImageMain: View {
    let data: Data?
    var body: some View {
        ZStack {
            if let data = data,
               let image = UIImage(data: data) {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFill()
                    .ignoresSafeArea()
                Rectangle()
                    .ignoresSafeArea()
                    .foregroundColor(.black.opacity(0.4))
            } else {
                Image("launchScreen")
                    .resizable()
                    .ignoresSafeArea()
            }
        }
        
    }
}

struct Buttons: View {
    @State private var isSharePresented: Bool = false
    @State private var isSharePresentedSave: Bool = false
    @State private var text: String = ""
    @State private var isFirstResponder: Bool = true

    var body: some View {
        HStack {
            Button {
                isSharePresented = true
            } label: {
                Image("shareBtn")
            }
            .popover(isPresented: $isSharePresented, content: {
                            if let image = takeScreenshot() {
                                ActivityViewController(activityItems: [image])
                            }
            })
            Spacer()
            Button {
                isSharePresentedSave = true
            } label: {
                Image("plusBtn")
            }.sheet(isPresented: $isSharePresentedSave, content: {
                SaveAdviceModule(text: $text, isFocused: $isFirstResponder, placeHolder: "Совет, блять...")
            })
            
        }
    }
}
//struct MainModule_Previews: PreviewProvider {
//    static var previews: some View {
//        MainModule()
//    }
//}
