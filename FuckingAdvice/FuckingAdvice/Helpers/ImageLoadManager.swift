//
//  ImageLoadManager.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 14.11.2021.
//

import Foundation

protocol ImageLoadManagerProtocol {
    func downloader(url: URL?, completion: ((Data?) -> Void)?, onError: ((Error) -> Void)?)
}

class ImageLoadManager: ImageLoadManagerProtocol {
    
    func downloader(url: URL?, completion: ((Data?) -> Void)?, onError: ((Error) -> Void)?) {
        guard let url = url else { return }
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let errorData = error {
                onError?(errorData)
            }
            completion?(data)
        }.resume()
    }
}
