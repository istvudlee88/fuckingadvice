//
//  AppFonts.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 14.11.2021.
//

import Foundation
import SwiftUI

//swiftlint:disable type_name
typealias fonts = AppFonts
struct AppFonts {
    
    // MARK: - bold
    static let bold42 = UIFont(name: "RobotoCondensed-Bold", size: 42)
    
    // MARK: - regular
    static let regular16 = UIFont(name: "RobotoCondensed-Regular", size: 16)
    
    static func normal(_ size: CGFloat) -> UIFont? {
        UIFont(name: "RobotoCondensed-Regular", size: size)
    }
    static func bold(_ size: CGFloat) -> UIFont? {
        UIFont(name: "RobotoCondensed-Bold", size: size)
    }

}

extension Font {
    static func normal(_ size: CGFloat) -> Font? {
        Font((fonts.normal(size) ?? UIFont().withSize(size)) as CTFont)
    }
    static func bold(_ size: CGFloat) -> Font? {
        Font((fonts.bold(size) ?? UIFont().withSize(size)) as CTFont)
    }
}
