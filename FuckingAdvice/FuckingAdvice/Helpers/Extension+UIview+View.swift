//
//  Extension+UIview/View.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 15.11.2021.
//

import Foundation
import SwiftUI

extension UIView {
    var renderedImage: UIImage? {
        let rect = self.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        if let context: CGContext = UIGraphicsGetCurrentContext() {
            DispatchQueue.main.async {
                self.layer.render(in: context)
            }
        }
        // get a image from current context bitmap
        guard let capturedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        return capturedImage
    }
}

extension View {
    func takeScreenshot() -> UIImage? {
        let window = UIWindow(frame: CGRect(origin: UIScreen.main.bounds.origin, size: UIScreen.main.bounds.size))
        let hosting = UIHostingController(rootView: self)
        hosting.view.frame = window.frame
        window.addSubview(hosting.view)
        window.makeKeyAndVisible()
        return hosting.view.renderedImage
    }
}
