//
//  Containers.swift
//  FuckingAdvice
//
//  Created by Анатолий Оруджев on 13.11.2021.
//

import Foundation
import Dip

class Containers {
    static var managersContainer = DependencyContainer(autoInjectProperties: true, configBlock: { container in
        // MARK: - network
        container.register(.singleton) { MainNetworkManager() as MainNetworkManagerProtocol }
        container.register(.unique) { ImageLoadManager() as ImageLoadManagerProtocol }

        // MARK: - business
        container.register(.unique) { MainViewModel() as MainViewModelProtocol }
    })
}

